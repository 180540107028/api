package com.example.api.activity;

import android.app.ProgressDialog;
import android.graphics.Movie;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.api.R;
import com.example.api.adapter.IndicaListAdapter;
import com.example.api.api.ApiClient;
import com.example.api.api.ApiInterface;
import com.example.api.indica_list.IndicaList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OptionalDataException;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import butterknife.Bindview;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityApiCall<> extends BaseActivity {

    ArrayList<IndicaList> indicaList = new ArrayList<>();
    @Bindview(R.id.rcvIndicaList)
    RecyclerView rcvIndicaList;

    IndicaListAdapter adapter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_api_call);
        ButterKnife.bind(this);
    }

    private void setContentView(int activity_api_call) {
    }

    void setIndicaListAdapter(){
        if (adapter == null) {
            adapter = new IndicaListAdapter(this,indicaList);
            rcvIndicaList.setLayoutManager(new GridLayoutManager(this,1));
            rcvIndicaList.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }
    }


    void getindicaListFromServer() {
        progressDialog = progressDialog.show(this,getString(R.string.app_name),.getString(R.string.lbl_pls_wait));
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.gethtmlindica(API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    parseApiDate(response.body().string());
                }catch (IOException e){
                    e.printStackTrace();
                }
                if (progressDialog != null && progressDialog.isShowing(){
                    progressDialog.dismiss();
                }
                setIndicaListAdapter();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private Object getString(int app_name) {
    }

    void parseApiDate(String jsonResponse){
        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);
            OptionalDataException array;
            for (int i = 0; i < array.length(); i++){
                JSONObject indicaObject = array.getJSONObject(i);
                IndicaList modal = new IndicaList();

                modal.setWord(indicaObject.getString("word"));
                modal.setNagari(indicaObject.getString("nagari"));
                modal.setDescription(indicaObject.getString("description"));
                modal.setCategory(indicaObject.getString("category"));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
