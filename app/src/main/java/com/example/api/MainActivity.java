package com.example.api;

import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    SearchView mySearchView;
    ListView myListView;

    ArrayList<String> list;
    ArrayList<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mySearchView = (SearchView)findViewById(R.id.searchView);
        myListView = (ListView)findViewById(R.id.myList);

        list.add("Word");
        list.add("Nagari");
        list.add("Description");
        list.add("Category");

        adapter = new ArrayAdapter<>(this,android:R.layout.support_simple_spinner_dropdown_item);
        myListView.setAdapter(adapter);

        mySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()){
            @Override
            public boolean onQueryTextSubmit(String Object s;s){
                return false;
            }
            @Override
            public boolean onQueryTextChange(String Object s;s){
                adapter.get().filter{s};
                return false;
            }

        }

    }

}