package com.example.api.indica_list;

import java.io.Serializable;

public class IndicaList implements Serializable {

     String word;
     String nagari;
     String description;
     String category;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getNagari() {
        return nagari;
    }

    public void setNagari(String nagari) {
        this.nagari = nagari;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "IndicaList{" +
                "word='" + word + '\'' +
                ", nagari='" + nagari + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

    public Object getTital() {
    }
}
