package com.example.api.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.api.R;
import com.example.api.indica_list.IndicaList;

import java.util.ArrayList;

public class IndicaListAdapter extends RecyclerView.Adapter<IndicaListAdapter.IndicaListHolder> {

    Context context;
    ArrayList<IndicaList> indicaList;

    public IndicaList(Context context, ArrayList<IndicaList> indicaList){
        this.context = context;
        this.indicaList = indicaList;
    }

    @NonNull
    @Override
    public IndicaListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new IndicaListHolder(LayoutInflater.from(context).inflate(R.layout.view_row_indica_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull IndicaListHolder holder, int position) {
        holder.tvTital.setText(indicaList.get(position).getTital());
        holder.tvName.setText(indicaList.get(position).getName());
        holder.tvRating.setText(String.format("%.2d",indicaList.get(position).getPopularrity()));
    }

    @Override
    public int getItemCount() {
        return indicaList.size();
    }

    class IndicaHolder extends RecyclerView.ViewHolder {
        @Bindview(R.id.tvTital)
        TextView tvTital;
        @Bindview(R.id.tvName)
        TextView tvName;
        @Bindview(R.id.tvRating)
        TextView tvRating;
        public IndicaHolder(@NonNull View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView );
        }
    }

    public class IndicaListHolder extends RecyclerView.ViewHolder {
        public IndicaListHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
