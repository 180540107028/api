package com.example.api.api;

import android.telecom.Call;
import androidx.room.Query;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("indica/html")
    Call<ResponseBody> gethtmlindica(@Query("api_key") String apiKey);

    @GET("indica/{id}")
    Call<ResponseBody> getindicaDetails(@Path("id") int id, @Query("api_key") String apiKey);
}
